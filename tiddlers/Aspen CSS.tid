created: 20180627121525083
modified: 20180719142416493
tags: 
title: Aspen CSS
type: text/css

<style>
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 260px; /* Margin bottom by footer height */
}
@media (min-width: 1200px) {
  .aspen .container {
      max-width: 1200px;
  }
}
.aspen p
{
    margin:0;
    padding:0;	
    text-align: justify;
    font-family: 'Lora', serif;
}
.aspen label, .aspen .list-unstyled li
{
	font-family: 'Lora', serif;
}
.aspen .container p
{
    margin-top: 0;
    margin-bottom: 1rem;
    text-align: justify;
}

.aspen h1, .aspen h2
{
	font-family: 'Raleway', sans-serif;
    font-size: 2rem;   
    font-weight: 700;
}
.aspen h6
{
	font-family: 'Cabin', sans-serif;
    font-size: 1.14rem;
}
.aspen .lh-1
{
	line-height:1em;
}
@media (min-width: 992px) {
  .show>.dropdown-menu {
	  left: 50%;
	  transform: translateX(-50%);
	}
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 260px; /* Set the fixed height of the footer here */
  background-color: #fff;
}
.footer p
{
  font-family: 'Raleway', sans-serif;
}
.text-grey
{
	color: #525252;
}
.text-teal
{
	color: #34bfbd;
}
.text-teal:hover, .text-teal:active
{
	color: #2ea6a4;
}
.text-yellow
{
	color:#f9db07;
}
.text-orange
{
	/*color:#ee8c00;*/
    color:#f78f1f;
}
.text-red
{
	/*color:#e32402;*/
    color: #ee2d24;
}
.text-linkedin
{
	color: #0077b5;
}
.text-facebook
{
	color: #3b5998;
}
.btn-facebook
{
	background: #3b5998;
}
.text-twitter
{
	color: #00aced;
}

.btn-orange
{
	background-color: #f78e1d;
}
.btn-orange:hover, .btn-orange:active
{
	background-color: #eb7400;
}
.btn-teal-light
{
	background-color: #62e9e7;
}
.btn-teal-light:hover, .btn-teal-light:active
{
	background-color: #59d4d2;
}
.btn-teal
{
	background-color: #53c5c3;
}
.btn-teal:hover, .btn-teal:active
{
	background-color: #4bb3b1;
}
.btn-teal-dark
{
	background-color: #44a1a0;
}
.btn-teal-dark:hover, .btn-teal-dark:active
{
	background-color: #3a8988;
}
/* CUSTOMIZE THE CAROUSEL
-------------------------------------------------- */

/* Carousel base class */
/* Since positioning the image, we need to help out the caption */
.carousel-caption {
 /* bottom: 3rem; */
  z-index: 10;
}
.carousel-caption h1, .basic-art h1, .basic-art h2
{
	font-family: 'Cabin', sans-serif;
    font-size: 3.0rem;
    line-height:3.3rem;
    text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4); 
}

@media (min-width: 768px) {
  .carousel-caption h1, .basic-art h1, .basic-art h2
  {
      font-family: 'Cabin', sans-serif;
      font-size: 4.4rem;
      line-height:4.6rem;
      text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4); 
  }
}

.aspen .asfg-art p
{
    text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4); 
}
.claims-art h1
{
    font-size: 4.4rem;
    line-height:4.6rem;
    text-shadow: 3px 4px 4px rgba(0, 0, 0, 0.8); 
}
.claims-art h2
{
    font-size: 2.8rem;
    line-height: 2.8rem;
    text-shadow: 2px 4px 4px rgba(0, 0, 0, 0.8); 
}
/* Declare heights because of positioning of img element */
.carousel-item {
  height: 15rem;
  background-color: #777;
  
}
@media (min-width: 768px) {
  /* Declare heights because of positioning of img element */
  .carousel-item {
    height: 20rem;
    background-color: #777;
  }
}
#newsCarousel .carousel-item {
  height: auto !important;
  text-align: center;
  background-color: #17a2b8!important;
}
.carousel-item > img {
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  min-width: 1300px;
}
.basic-art
{
	background: center center;
  	background-repeat: no-repeat;
	-webkit-background-size: cover;
  	-moz-background-size: cover;
  	-o-background-size: cover;
  	background-size: cover;
}
.contact-art
{
	background-image: url(../images/aspen/contact-banner.jpg); 
}
.marketing-art
{
	background-image: url(../images/aspen/marketing-banner.jpg); 
}
.auto-art
{
	background-image: url(../images/aspen/auto-banner.jpg); 
}
.policy-art
{
	background-image: url(../images/aspen/policy-banner.jpg); 
}
.home-art
{
	background-image: url(../images/aspen/home-banner.jpg); 
}
.homeowner-art
{
	background-image: url(../images/aspen/homeowner-banner.jpg); 
}
.manufactured-art
{
	background-image: url(../images/aspen/manufactured-banner.jpg); 
}
.dwelling-art
{
	background-image: url(../images/aspen/dwelling-banner.jpg); 
}
.condominium-art
{
	background-image: url(../images/aspen/condominium-banner.jpg); 
}
.about-art
{
	background-image: url(../images/aspen/behind-name-banner.jpg); 
}
.giving-back-art
{
	background-image: url(../images/aspen/giving-back-banner.jpg); 
}
.asfg-art
{
	background-image: url(../images/aspen/asfg-banner.jpg); 
}
.become-agent-art
{
	background-image: url(../images/aspen/become-agent-banner.jpg); 
}
.agents-advantage-art
{
	background-image: url(../images/aspen/agents-advantage-banner.jpg); 
}
.careers-art
{
	background: center bottom;
    	background-repeat: no-repeat;
	-webkit-background-size: cover;
  	-moz-background-size: cover;
  	-o-background-size: cover;
  	background-size: cover;
	background-image: url(../images/aspen/careers-banner.jpg); 
}
.claims-art
{
background: center bottom;
    	background-repeat: no-repeat;
	-webkit-background-size: cover;
  	-moz-background-size: cover;
  	-o-background-size: cover;
  	background-size: cover;
	background-image: url(../images/aspen/claims-banner.jpg); 
}
.navbar .navbar-nav li .dropdown-menu a:focus 
{
      color: #ffffff !important;
      background-color: #f78e1d;
      -webkit-box-shadow: none;
      -moz-box-shadow: none;
      box-shadow: none;
}

@media (min-width: 992px) { 
	.nav-item, .dropdown-item  {
	  font-size: .9rem;
	}
}
@media (min-width: 1200px) { 
	.nav-item, .dropdown-item  {
	  font-size: 1rem;
	}
}

.card-deck-aspen .card-header
{
	border-radius: 0;
	background-color: #fff;
    border-bottom: none;
}
.card-deck-aspen .card-header h4
{
	font-family: 'Raleway', sans-serif;
    font-size: 1.2rem;   
    font-weight: 700;
}
.card-deck-aspen .card-header hr
{
	display: block;
    height: 3px;
    border: 0;
    border-top: 3px solid #53c5c3;
    margin: 1em 0;
    padding: 0;
}
.card-deck-aspen li::before {
  content: "•"; 
  color: #f78e1d;
  display: inline-block; 
  width: 1em;
  margin-left: -1em}
.aspen main .list-unstyled li::before {
  content: "•"; 
  color: #f78e1d;
  display: inline-block; 
  width: 1em;
  margin-left: -1em}
  
  
  /*----Parallax setup----------*/



section.module.content {
  padding: 20% 0;
}
section.module.parallax {
  height: 500px;
  margin:0;
  background-position: 50% 50%;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
section.module.parallax-forest {
  background-color: #f5dc6b;
  background-image: url(../images/aspen/forest.jpg);
}
section.module.parallax-forest h2
{
	font-family: 'Cabin', sans-serif;
    font-size: 3.8rem;
    line-height:4.0rem;
    text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4); 
    padding: 15% 0;
}
@media (min-width: 768px) {
  section.module.parallax-forest h2
  {
      font-family: 'Cabin', sans-serif;
      font-size: 4.6rem;
      line-height:4.8rem;
      text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4); 
      padding: 15% 0;
  }
}
</style>