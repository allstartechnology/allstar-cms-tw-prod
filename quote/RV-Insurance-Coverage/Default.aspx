﻿<%@ Page Language="C#" MasterPageFile="_Section.master" %>

<asp:content contentplaceholderid="head" runat="server">
	<title>RV Insurance Coverage | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Coverage, RV Insurance Coverages" />
	<meta http-equiv="Description" name="Description" content="Offering RV insurance coverage including innovative and specialty RV insurance coverages that goes above and beyond the standard coverage most RV insurance companies provide." />
	<style type="text/css">
		div.content { height: 780px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Overview').addClass("Active"); });
  </script>
    <!-- Start Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function () {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push(
        { qacct: "p-VypNfW3as3_Ed", labels: "_fp.event.Insurance Converage" }
        );
    </script>
    <noscript>
    <img src="//pixel.quantserve.com/pixel/p-VypNfW3as3_Ed.gif?labels=_fp.event.Insurance+Converage" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
    </noscript>
    <!-- End Quantcast tag -->
</asp:content>

<asp:Content ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/totalloss_coverages_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ContentPlaceHolderID="main_title" runat="server">
	RV Insurance Coverage
</asp:Content>
			
<asp:Content ContentPlaceHolderID="main_details" runat="server">
	<div class="items" style="height:auto">
		<ul> 
			<li><a href="Innovative">Innovative RV Insurance coverage</a> is available exclusively with Blue Sky RV such as: Depreciation Free, Total Loss Deductible Buy-Back, and Diminishing Deductible Express</li>
			<li><a href="Specialty">Specialty RV insurance coverages</a> include Total Loss Replacement, Diminishing Deductibles, Personal Effects, and more</li>
			<li><a href="Standard">Standard RV Insurance Coverages</a> include Liability protection such as: Bodily Injury, Property Damage, Uninsured Motorists, Medical Payments, and Personal Injury Protection</li>
			<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/motorhome-insurance.aspx">Motor Homes</a> and <a href="<%= Application["AppPath"] %>/recreational-vehicles/bus-conversion-insurance.aspx">Bus Conversions</a> from $20,000 in value to $2,000,000 in value; <a href="<%= Application["AppPath"] %>/recreational-vehicles/travel-trailer-insurance.aspx">Travel Trailers</a> and <a href="<%= Application["AppPath"] %>/recreational-vehicles/fifth-wheel-insurance.aspx">Fifth Wheels</a> from $5,000 in value to $200,000 in value</li>                            
		</ul>
		<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RV Insurance Coverage Features</h3>
		<ul>
					<li>24-Hour <a href="<%= Application["AppPath"] %>/rv-insurance-coverage/rv-roadside-assistance.aspx">RV Roadside Assistance</a></li>
			<li>Commercial Use</li>
			<li>Motor Homes and Travel Trailers registered to LLCs, Partnerships, Trusts, and Corporations</li>
			<li>Discounts for membership in RV Associations and Manufacturer’s Clubs</li>
			<li>Multi-Vehicle Discounts</li>                                                      
		</ul>                     
	</div>
</asp:Content>

