﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Emergency Expense | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, emergency expense">
	<meta http-equiv="Description" name="Description" content="How would you cover Emergency Expense incurred after a vehicle loss or breakdown? The Blue Sky RV Insurance program’s Emergency Expense coverage provides a range of options as part of the specialty RV insurance coverage options.">
	<style type="text/css">
		div.content { height: 750px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Emergency-Expense').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/Emergency_Expence_coverages_bg.jpg" alt="Specialty RV Insurance – Emergency Expense" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Emergency Expense RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>The Blue Sky RV Insurance program offers Emergency Expense coverage to provide for any emergency expense incurred after a vehicle loss or breakdown. Emergency expense basic limit of $750 applies with optional limits of $1,500. In addition, a $5,000 limit is available to our Full Timers.</p>
</asp:Content>