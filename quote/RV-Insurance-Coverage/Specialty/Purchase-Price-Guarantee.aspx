﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Purchase Price Guarantee | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, purchase price guarantee">
	<meta http-equiv="Description" name="Description" content="With the Purchase Price Guarantee Coverage available in the specialty RV insurance program, Blue Sky RV Insurance will pay the purchase price towards the replacement unit in the event of a total loss.">
	<style type="text/css">
		div.content { height: 805px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Purchase-Price-Guarantee').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/Purchase_Price_Guarantee_coverages_bg.jpg" alt="Specialty RV Insurance – Purchase Price Guarantee" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Purchase Price Guarantee<br />RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Purchase price guarantee protects against depreciation by replacing the loss settlement options in the policy. In the event of a total loss, we will pay the purchase price toward the purchase of a replacement unit. Purchase price guarantee is available to policyholders that have purchased a used motorhome, travel trailer, fifth wheel camper or other recreational vehicle during the past 12 months.  The recreational vehicle must be less than 10 model years old at the time this coverage is initially purchased and the coverage automatically drops off at the first renewal after the unit
is 10 model years old.</p>
</asp:Content>