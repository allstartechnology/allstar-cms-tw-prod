﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance | Agreed Value | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV Insurance, Agreed Value, Bus Conversion Insurance ">
	<meta http-equiv="Description" name="Description" content="Blue Sky offers bus conversion insurance on an Agreed Value basis. Agreed Value Coverage requires a certified RV insurance appraisal.">
	<style type="text/css">
		div.content { height: 750px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Agreed-Value').addClass("Active"); });
  </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/Agreed_Value_coverages_bg.jpg" alt="Specialty RV Insurance – Agreed Value" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Agreed Value RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Bus Conversion insurance may be provided on an Agreed Value basis, which provides physical damage protection up to the agreed value limit toward the replacement of a unit. Agreed Value Coverage from Blue Sky RV Insurance requires a certified appraisal of the bus conversion within the last 3 years.</p>
</asp:Content>
