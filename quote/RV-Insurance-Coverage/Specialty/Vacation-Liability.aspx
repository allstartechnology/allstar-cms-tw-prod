﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content contentplaceholderid="head" runat="server">
	<title>RV Insurance | Vacation Liability | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, vacation liability">
	<meta http-equiv="Description" name="Description" content="Vacation Liability, or Campsite Liability as it is also known, provides personal liability coverage if you are using your Recreational Vehicle as a vacation residence.  This and other specialty RV Insurance coverage options are available with the Blue Sky RV Insurance program.">
	<style type="text/css">
		div.content { height: 720px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Vacation-Liability').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/specialty_rv_insurance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ContentPlaceHolderID="main_title" runat="server">
	Vacation Liability RV Insurance
</asp:Content>
			
<asp:Content ContentPlaceHolderID="main_details" runat="server">
	<p>Also known as Campsite Liability Insurance, Vacation Liability provides personal vacation liability insurance when using your recreational vehicle as a vacation residence. Travel with the assurance that your RV is insured for vacation liability. Vacation Liability limits are available up to $500,000.</p>
</asp:content>