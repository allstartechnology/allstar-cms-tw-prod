﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content contentplaceholderid="head" runat="server">
	<title>RV Insurance | Adjacent Structures | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, adjacent structures">
	<meta http-equiv="Description" name="Description" content="Blue Sky RV Insurance provides Adjacent Structures Coverage for any structures and their contents next to your RV.">
	<style type="text/css">
		div.content { height: 720px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Adjacent-Structures').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/specialty_rv_insurance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ContentPlaceHolderID="main_title" runat="server">
	Adjacent Structures RV Insurance
</asp:Content>
			
<asp:Content ContentPlaceHolderID="main_details" runat="server">
	<p>Adjacent Structures coverage provides protection for structures set up next to your RV such as screen rooms and sheds, and provides coverage for the contents of the adjacent structure as well.  The Blue Sky RV Insurance program now offers up to $10,000 of protection.</p>
</asp:Content>