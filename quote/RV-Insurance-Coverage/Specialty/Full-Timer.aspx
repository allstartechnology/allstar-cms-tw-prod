﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance | Full Timer | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, full timer RV insurance">
	<meta http-equiv="Description" name="Description" content="The Blue Sky RV Insurance program provides Full Timer RV Insurance which covers you like a homeowner’s policy would if you use your RV as a residence more than 5 months out of the year.">
	<style type="text/css">
		div.content { height: 1040px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Full-Timer').addClass("Active"); });
  </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/rv-roadside-assistance_bg.jpg" alt="Specialty RV Insurance – Full Timer RV Insurance" />
</asp:Content>
		
<asp:Content ContentPlaceHolderID="main_title" runat="server">
	Full Timer RV Insurance
</asp:Content>
			
<asp:Content ContentPlaceHolderID="main_details" runat="server">
	<p>Full Timer RV Insurance provides protection to policyholders that use their RV as a residence more than 5 months out of the year or don't own or lease a residence. </p>

	<h2>Full Timer RV Insurance - Liability</h2>
	<p>Full Timer RV Insurance includes third party liability protection.  The Blue Sky RV Insurance program can provide limits up to $500,000.</p>

	<h2>Full Timer RV Insurance – Medical Payments</h2>
	<p>The Blue Sky RV Insurance program also offers Medical Payments for full timers, which provides payment for medical expenses incurred by a third party in a non-vehicle accident, and is available with limits of $10,000 per person, $20,000 per accident.</p>

	<h2>Full Timer RV Insurance – Secured Storage</h2>
	<p>Insurance for personal property kept in a commercial storage facility is also available with limits up to $99,000.</p>
</asp:Content>
