﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content contentplaceholderid="head" runat="server">
	<title>RV Insurance | Personal Effects | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, personal effects">
	<meta http-equiv="Description" name="Description" content="Personal Effects Coverage protects your belongings inside or outside your RV by paying the replacement cost for items such as clothing, jewelry, cameras, and cooking equipment.  This specialty RV insurance coverage option is available in most states through the Blue Sky RV Insurance program.">
	<style type="text/css">
		div.content { height:790px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Personal-Effects').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/specialty_rv_insurance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Personal Effects RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Protect your personal effects inside or outside your Recreation Vehicle with Personal Effects coverage. Personal Effects coverage will pay the replacement cost for damage to, or loss of, contents used in conjunction with your RV such as clothing, jewelry, cameras, cooking equipment, and other personal effects not permanently attached to the RV. Limits up to $99,000 are provided. A limitation of $1,000 applies per item or group of similar items; however Valuable Personal Property Coverage may be purchased for items that exceed this $1,000 sublimit.</p>
</asp:content>
