﻿<%@ Page Language="C#" MasterPageFile="_Innovative.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Diminishing Deductible Express | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV insurance, innovative RV insurance, diminishing deductible express" />
	<meta http-equiv="Description" name="Description" content="Blue Sky RV Insurance offers Innovative RV insurance products for recreational vehicles. Blue Sky RV Insurance is proud to offer diminishing deductible express earning the full reward in half the time!" />
	<style type="text/css">
		div.content { height: 940px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#DiminishingDeductibleExpress').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
		<img src="<%= Application["AppPath"] %>/images/Diminishing_Deductible_Express_coverages_bg.jpg" alt="Innovative RV Insurance - Diminishing Deductible Express" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
		Diminishing Deductible Express<br />RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
		<p>The Blue Sky RV Insurance program has put years of RV insurance experience to work in designing exciting and innovative RV insurance coverage options such as Diminishing Deductible Express – earning your claims free reward in half the time of most RV insurance policies!</p>
		<p>With the Blue Sky Diminishing Deductible Express option, every loss-free year your Comprehensive and Collision deductibles are reduced by 50%. After two years of loss-free experience, your deductible is reduced to $0 in the event of a loss to your recreational vehicle.</p>
		<h2>RV Insurance Claim Example</h2>
		<p>While travelling down the highway, a deer runs in front of your recreational vehicle causing extensive damage.  This is the first claim you have had to make in the 2 years since purchasing the RV.   Traditional RV Insurance would require payment of your $500 comprehensive deductible as part of the loss settlement.  The Diminishing Deductible Express option offered by the Blue Sky RV Insurance program would have reduced your deductible by 50% the first loss free year and the remaining 50% for the second loss free year.  Since there has been 2 consecutive years with no comprehensive claims, you are out of pocket $0 – a savings of $500 over traditional RV Insurance!</p>
</asp:Content>