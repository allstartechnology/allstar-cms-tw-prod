﻿<%@ Page Language="C#" MasterPageFile="../_Section.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Standard RV Insurance | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Coverage, RV Insurance Coverages" />
	<meta http-equiv="Description" name="Description" content="Offering RV insurance coverage including innovative and specialty RV insurance coverages that goes above and beyond the standard coverage most RV insurance companies provide." />
	<style type="text/css">
		div.content { height: 800px; }
		div.text { width: 800px; margin-top: 0px; }
		div.text p { margin: 8px 0; }
		div.text h1 { margin: 4px 0 10px 0; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Standard').addClass("Active"); });
	</script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
		<img src="<%= Application["AppPath"] %>/images/rvprotective_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Standard RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>
		<span style="color: #3186a9; font-weight: bold;">Bodily Injury & Property Damage</span><br />
		BI/PD covers your legal liability to a 3rd party as the result of an accident involving your motorized vehicle. This standard RV insurance, required in most states, will pay medical bills and lost wages of the injured party, as well as damage to property such as another vehicle. BI/PD limits up to $500,000 are available for any motorized unit. BI/PD limits up to $1,000,000 are available for
motorized units over $200,000 in value.
	</p>

	<p>
		<span style="color: #3186a9; font-weight: bold;">Medical Payments</span><br />
		Medical Payments will pay medical costs to you or a family member as the result of an accident in your RV, regardless of who is at fault.
	</p>

	<p>
		<span style="color: #3186a9; font-weight: bold;">Uninsured & Underinsured Motorists</span><br />
		When a person at fault for an accident does not have insurance or does not have enough insurance to cover your damages, Uninsured & Underinsured Motorists (UM/UIM) provides protection to you. This includes medical bills and lost wages and may include damage to property, including your RV.
	</p>

	<p>
		<span style="color: #3186a9; font-weight: bold;">Comprehensive and Collision</span><br />
		Comprehensive (Comp or Other Than Collision) and Collision cover the cost to repair your RV if it is stolen or damaged in a covered accident.  Various deductible options are available for standard RV insurance and may be combined with <a href="../specialty/Diminishing-Deductible.aspx">Diminishing Deductible</a> or Blue Sky’s new <a href="../innovative/Diminishing-Deductible-Express.aspx">Diminishing Deductible Express</a> option.
	</p>

	<p>
		<span style="color: #3186a9; font-weight: bold;">Towing & Roadside Labor</span><br />
		If your RV becomes disabled, Blue Sky’s Towing & Roadside Labor will provide for reasonable expense to tow your disabled vehicle to the nearest qualified repair facility as well as provide roadside labor at the point of disablement.
	</p>

	<p>
		<span style="color: #3186a9; font-weight: bold;">Mexico Coverage</span><br />
		Blue Sky offers Mexico Insurance Protection for your RV.  This coverage will extend your standard RV insurance coverage territory to include physical damage losses (Comprehensive and Collision) in Mexico.  Mexican liability coverage must be purchased from a licensed Mexican liability insurer and the unit must be repaired in the United States.
	</p>

	
</asp:Content>