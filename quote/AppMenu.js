﻿
var currentMenuID = 0;
var gTimeOut = null;

function delay(id, callback) {
	if (currentMenuID) hideMenu(currentMenuID);
	var timeout = null;
	timeout = setTimeout(callback, 300);
	$('#Tab' + id + ' a').onmouseout = function () {
		// Clear any timers set to timeout
		clearTimeout(timeout);
		hideMenu(id);
	}
}

function displayMenu(id) {
	$('#SubMenu' + id).css({ "display": "block" });
	var x = $('#Tab' + id + ' a').position().left;
	var y = 46;
	$('#SubMenu' + id).css({ "left": x, "top": y });
	if (!$('#Tab' + id + ' a').hasClass("Selected")) $('#Tab' + id).addClass("Hovered");
	currentMenuID = id;
}

function hideMenu(id) {
	gTimeOut = window.setTimeout("forceHideMenu(" + id + ")", 300);
}

function forceHideMenu(id) {
	$('#SubMenu' + id).css("display", "none");
	$('#Tab' + id).removeClass("Hovered");
}

function cancelHideMenu(id) {
	if (gTimeOut) window.clearTimeout(gTimeOut);
}

