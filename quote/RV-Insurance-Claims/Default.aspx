<%@ Page Language="C#" MasterPageFile="~/App.master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance Claims | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV insurance claim, RV insurance claims, Blue Sky RV Insurance" />
	<meta http-equiv="Description" name="Description" content="Accidents and losses happen. Fortunately, a Blue Sky RV Insurance Claim Coverage Professional is standing by to handle your RV claim promptly and efficiently." />
	<style type="text/css">
		.text {
			position: absolute;
			top: 25px;
			left: 50px;
			width: 400px;
			text-decoration: none;
			line-height: 19px;
		}
		.text li {
			line-height: 20px;
		}
		.text h2 {
			font-size: 20px;
		}
		.content {
			height: 700px;
		}
		#ClaimSteps {
			position: absolute;
			top: 0px;
			left: 490px;
			width: 370px;
			text-decoration: none;
			line-height: 19px;
		}
		#ClaimSteps div {
			font-size: 88%;
			line-height: 16px;
			color: #000;
		}
		#ClaimSteps h2 {
			color: #cc0000;
		} 
	</style>
	<script type="text/javascript">
		$(document).ready(function () { $('#Tab7').addClass("selected"); });
	</script>
    <!-- Start Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function () {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push(
        { qacct: "p-VypNfW3as3_Ed", labels: "_fp.event.Insurance Claims" }
        );
    </script>
    <noscript>
    <img src="//pixel.quantserve.com/pixel/p-VypNfW3as3_Ed.gif?labels=_fp.event.Insurance+Claims" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
    </noscript>
    <!-- End Quantcast tag -->
</asp:Content>

<asp:Content ContentPlaceHolderID="main" runat="server">
	<div class="content">
		<div class="stuff">
			<div class="company_info">
				<img src="<%= Application["AppPath"] %>/images/claims_bg.jpg" alt="RV Insurance Claims" />
			</div>
			<div class="text">
				<h1>RV Insurance Claims</h1>
				<p>
					When an accident or loss happens to your motorhome, travel trailer, fifth wheel camper or other recreational 
          vehicle, please report it as soon as possible. Your RV insurance claim will be assigned to a Blue Sky RV 
          Insurance Coverage Professional who has the training and expertise to handle any RV insurance claims 
          promptly and efficiently.
				</p>
				<h2>Reporting an RV Insurance Claim</h2>
				<div class="claims_items">
					<ul>
						<li>Call <span style="font-weight: bold; font-size: 12px;">(888) 665-3719</span> to report all RV insurance Claims</li>
						<li>With the Blue Sky RV Claims Line, you can report a claim 24 hours a day, 7 days a week!</li>
						<li><span style="text-align: left;">Email us (non-emergency claims only):  <a href="mailto:claims@blueskyrvinsurance.com">claims@blueskyrvinsurance.com</a></span></li>
					</ul>
				</div>
				<h2>Information Needed to Report RV<br />
					Insurance Claims</h2>
				<div class="claims_items">
					<ul>
						<li>Date and location of the accident or loss</li>
						<li>Your policy number and recreational vehicle information</li>
						<li>Names, phone numbers, and insurance information of other people involved in the occurrence</li>
						<li>Descriptions of how the accident happened, the damage to all vehicles, and any injuries</li>
						<li>Witness information</li>
						<li>Names and report numbers for any officials (police, fire, or ambulance) responding to the scene of the loss</li>
					</ul>
				</div>
				<span style="color:#cc0000; font-weight: bold; font-size: 14px;">
					Please report your RV insurance claims as soon as possible even if you 
          are missing some of the above information.
				</span>
			</div>
			
			<div class="text" id="ClaimSteps">
				<h2>What to Do After an RV Accident or Loss</h2>
				<div style="margin-left:-15px; margin-top:-6px; color:#333">
					<ol>
						<li>Contact the authorities, such as the police or fire department, even on minor accidents or fire damage</li>
						<li>If someone is injured, call for medical assistance</li>
						<li>If possible, move vehicles to safety and out of the way of traffic</li>
						<li>Take note of damage to the vehicles and, if you have a camera handy, take some photos to document the damage</li>
						<li>Exchange contact and insurance information with the other driver and obtain contact information for any witnesses</li>
						<li>Do not sign any documents and do not discuss the loss with anyone other than the police, your RV Insurance agent, or your Blue Sky RV Insurance claims representative</li>
						<li>Report the claim as soon as possible:  Call <span style="font-weight: bold;">(888) 665-3719</span></li>
					</ol>
				</div>
			</div>

		</div>
	</div>
</asp:Content>
