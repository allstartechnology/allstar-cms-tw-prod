<%@ Page Language="C#" MasterPageFile="~/App.master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Nation | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Nation, Blue Sky RV Insurance" />
	<meta http-equiv="Description" name="Description" content="RV Nation provides resources for RV Enthusiasts." />
	<script type="text/javascript">
		$(document).ready(function () { $('#Tab5').addClass("selected"); });
	</script>
	<style type="text/css">
		.text {
			position: absolute;
			top: 10px;
			left: 55px;
			width: 170px;
			text-decoration: none;
		}
	</style>
    <!-- Start Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function () {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push(
        { qacct: "p-VypNfW3as3_Ed", labels: "_fp.event.RV Nation" }
        );
    </script>
    <noscript>
    <img src="//pixel.quantserve.com/pixel/p-VypNfW3as3_Ed.gif?labels=_fp.event.RV+Nation" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
    </noscript>
    <!-- End Quantcast tag -->
</asp:Content>

<asp:Content ContentPlaceHolderID="main" runat="server">
	<div class="content" style="height:740px">
		<div class="stuff">
			<div class="company_info">
				<img src="<%= Application["AppPath"] %>/images/rvnation_bg.jpg" alt="RV Nation" />
			</div>
			<div class="text" style="width: 700px;">
				<h1>RV Nation</h1>
				Come check out RV Nation on Facebook & Twitter...
				<br/>  Where we provide RV Travel Tips & other fun stuff!
				<br/><br/>
                 <iframe src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fblueskyrvinsurance&width=500&colorscheme=light&show_faces=false&border_color&stream=true&header=false&height=550" scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:500px; height:550px; background: white; float:left; " allowtransparency="true"></iframe>
				<!--<br/>
				<br/>  We are running a Photo Contest! &nbsp;<a href="../contest">Click here</a> to check it out...
				<a href="<%= Application["AppPath"] %>/contest">
					<img src="<%= Application["AppPath"] %>/contest/images/contest_header.jpg" alt="PicPerfect RV Destinations" width="500" style="margin-top: 24px; border: 1px solid #999" /></a>-->
			</div>
		</div>
	</div>
</asp:Content>