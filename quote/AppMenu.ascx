﻿<%@ Control Language="C#" ClassName="AppMenu" %>

<!--Tabs-->
<div class="menu_item_home" id="Tab1"><a href="<%= Application["AppPath"] %>/default.aspx"><div class="menu_item" >home</div></a></div>
<div class="menu_item_about" id="Tab2" onmouseover="delay(2,'displayMenu(2)');" onmouseout="hideMenu(2);"><a href="<%= Application["AppPath"] %>/about-us"><div class="menu_item" >about us</div></a></div>
<div class="menu_item_coverages" id="Tab3" onmouseover="delay(3,'displayMenu(3)');" onmouseout="hideMenu(3);"><a href="<%= Application["AppPath"] %>/rv-insurance-coverage"><div class="menu_item">coverages</div></a></div>
<div class="menu_item_vehicle" id="Tab4" onmouseover="delay(4,'displayMenu(4)');" onmouseout="hideMenu(4);"><a href="<%= Application["AppPath"] %>/recreational-vehicles"><div class="menu_item">vehicles</div></a></div>
<div class="menu_item_rvnation" id="Tab5"><a href="<%= Application["AppPath"] %>/rv-nation"><div class="menu_item">rv nation</div></a></div>
<div class="menu_item_rfq" id="Tab6"><a href="<%= Application["AppPath"] %>/rv-insurance-quote-intro.aspx"><div class="menu_item">request a quote</div></a></div>
<div class="menu_item_claims" id="Tab7"><a href="<%= Application["AppPath"] %>/rv-insurance-claims"><div class="menu_item">claims</div></a></div>
<div class="menu_item_contact" id="Tab8"><a href="<%= Application["AppPath"] %>/contact-us"><div class="menu_item">contact us</div></a></div>

 <!--SubMenus-->
<div class="SubMenu" id="SubMenu2" onmouseover="cancelHideMenu(2);" onmousemove="cancelHideMenu(2);" onmouseout="hideMenu(2);">
	<ul>
		<li><a href="<%= Application["AppPath"] %>/about-us"><div>Company Information</div></a></li>
    <li><a href="<%= Application["AppPath"] %>/about-us/who-we-represent.aspx"><div>Who We Represent</div></a></li>
    <li><a href="<%= Application["AppPath"] %>/about-us/license-numbers.aspx" style="height:18px"><div>Agency License Numbers</div></a></li>
	</ul>
</div> 

<div class="SubMenu" id="SubMenu3" onmouseover="cancelHideMenu(3);" onmousemove="cancelHideMenu(3);" onmouseout="hideMenu(3);">
	<ul>
		<li><a href="<%= Application["AppPath"] %>/rv-insurance-coverage"><div>Overview</div></a></li>
    <li><a href="<%= Application["AppPath"] %>/rv-insurance-coverage/innovative"><div>Innovative RV Insurance</div></a></li>
    <li><a href="<%= Application["AppPath"] %>/rv-insurance-coverage/specialty"><div>Specialty RV Insurance</div></a></li>
    <li><a href="<%= Application["AppPath"] %>/rv-insurance-coverage/standard" style="height:18px"><div>Standard RV Insurance</div></a></li>
  </ul>
</div>

<div class="SubMenu" id="SubMenu4" onmouseover="cancelHideMenu(4);" onmousemove="cancelHideMenu(4);" onmouseout="hideMenu(4);">
  <ul>		
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/motorhome-insurance.aspx"><div>Class A Motorhome</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/mini-motorhome-insurance.aspx"><div>Mini-Motorhome (Class C)</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/camper-van-insurance.aspx"><div>Camper Van</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/bus-conversion-insurance.aspx"><div>Bus Conversion</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/medium-duty-tow-vehicle-insurance.aspx"><div>Medium Duty Tow Vehicle</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/toterhome-insurance.aspx"><div>Toterhome</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/travel-trailer-insurance.aspx"><div>Conventional Travel Trailer</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/fifth-wheel-insurance.aspx"><div>Fifth Wheel Camper</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/pop-up-camper-insurance.aspx"><div>Pop Up Camper</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/truck-camper-insurance.aspx"><div>Truck Camper</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/park-model-travel-trailer-Insurance.aspx"><div>Park Model Travel Trailer</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/utility-trailer-insurance.aspx"><div>Utility Trailer</div></a></li>
		<li><a href="<%= Application["AppPath"] %>/recreational-vehicles/personalauto-insurance.aspx" style="height:18px"><div>Personal Auto</div></a></li>
	</ul>
</div>


