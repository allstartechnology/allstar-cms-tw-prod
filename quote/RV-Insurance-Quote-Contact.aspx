<%@ page language="C#" masterpagefile="~/AppQuote.master" autoeventwireup="true" inherits="rfqcontact, App_Web_nrmhwma5" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>RV Insurance Quote | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote, Cheap RV Insurance" />
	<meta http-equiv="Description" name="Description" content="Request an RV Insurance Quote from Blue Sky RV Insurance." />
    
	<script type="text/javascript">
	    $(document).ready(function () { $('#Tab6').addClass("selected"); });
	    window.onbeforeunload = confirmExit;
	    alertStatus = true;
	    function confirmExit() {
	        if (alertStatus == true) return "All the information that you've entered will be lost.";
	    }
	</script>
    <%--<script type="text/javascript" src="<%= Application["AppPath"] %>/jquery.maskedinput.js"></script>


    <script type="text/javascript">
        jQuery(function ($) {
            $(".date").mask("99/99/9999");
            $(".phone").mask("999-999-9999");
        });
    </script>--%>
	<style type="text/css">
        tr.emailMe, tr.callMe
		{
			display:none;
		}
	</style>
   

   <%-- <script type="text/javascript">
        function hearAbout(val) {
            var element = document.getElementById('about');
            if (val == ' ' || val == 'Other')
                element.style.display = 'block';
            else
                element.style.display = 'none';
        }

    </script>  --%>


</asp:Content>
<asp:Content ContentPlaceHolderID="hero" runat="server">
    <section class="full_background clouds_mountain scenic_background">
	    <div class="bl_trans_bg">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <h4>Blue Sky RV Insurance Quote</h4>
                        <p>The Blue Sky RV Insurance program is currently not available for vehicles registered in the following states: AL, AK, CT, DE, FL, HI, KY, MA, MD, ME, MS, NH, NY, RI, VT and WY. If your registration state is listed above please check back soon as we are continuing to expand and add new states to serve you. Coverage is also not available on stationary trailers or park model travel trailers that are registered or garaged in FL, GA, LA, NC, NJ, SC, TX, and VA. </p>
                    </div>
                </div>
            </div>
	    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">
          <asp:Panel ID="pnlErrors" runat="server" Visible="false">
		<script type="text/javascript">
		    document.getElementById('bodyOveride').style.overflow = 'hidden';
		    document.getElementById('htmlOveride').style.overflow = 'hidden';
		</script>

        


		<table class="floatingTable u-full-width" border="0">
			<tr>
				<td>
					<div class="floatingPanel">
					    <asp:Button ID="cmdFloatClose" runat="server" Text="X" CssClass="common_button" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
						<asp:Panel ID="pnlDefault" runat="server" Visible="true">
							<div class="floatingPanelHeader">
							</div>
							<div class="floatingPanelContentStop">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentText">
									<asp:Literal ID="litErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<asp:Panel ID="pnlAlt" runat="server" Visible="false">
							<div class="floatingPanelHeaderAlt">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentTextAlt">
									<asp:Literal ID="litNoErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>

					</div>
				</td>
			</tr>
		</table>
	</asp:Panel>
    <h4>I prefer to be reached by:</h3>
    <div class="row">
        
        <div class="three columns">
            <asp:RadioButton CssClass="contactRadio" ID="emailMe" runat="server" GroupName="contactButtons" />
            <span class="label-body">Email</span>
        </div>
        <div class="three columns">
            <asp:RadioButton CssClass="contactRadio" ID="callMe" runat="server" GroupName="contactButtons"/>
            <span class="label-body">Telephone</span>
        </div>
    </div>
    <div class="row emailMe">
        <div class="twelve columns">
            <label>Email Address</label>
            <asp:TextBox placeholder="test@mailbox.com" ID="TextBox3" runat="server" MaxLength="100" CssClass="u-full-width rfq_text_field" />
        </div>
    </div>
    <div class="row callMe">
        <h4>Best time I can be reached:</h4>
    </div>
    <div class="row callMe">
        <div class="four columns">
            <label>Best Day</label>
            <asp:TextBox placeholder="XX/XX/XXXX" ID="datepicker" runat="server" MaxLength="30" CssClass="u-full-width" />
        </div>
        <div class="four columns">
            <label>Time of Day</label>
            <asp:DropDownList ID="DropDownList2" runat="server"
			CssClass="rfq_text_field u-full-width">
				<asp:ListItem Text="Morning" Value="Morning" />
				<asp:ListItem Text="Afternoon" Value="Afternoon" />
			</asp:DropDownList>
        </div>
        <div class="four columns">
            <label>Daytime Phone Number</label>
            <asp:TextBox placeholder="XXX-XXX-XXXX" ID="TextBox4" runat="server" MaxLength="30"  type="tel" CssClass="u-full-width" />
        </div>
    </div>
	<script type="text/javascript" src="moment.js"></script>
	<script type="text/javascript" src="pikaday.js"></script>
    <script type="text/javascript">

        var picker = new Pikaday(
        {
            field: document.getElementById('main_datepicker'),
            firstDay: 0,
            format: 'L',
            disableWeekends: true,
            minDate: new Date(),
            maxDate: new Date('2020-12-31'),
            yearRange: [2015, 2020]
        });

    </script>

	<script type="text/javascript">
		$(document).ready(function () {
			$("input[type='radio']").click(function () {
				$('.callMe').css('display', ($(this).val() === 'callMe') ? 'block' : 'none');
				$('.emailMe').css('display', ($(this).val() === 'emailMe') ? 'block' : 'none');
			});
		});
	</script>
    <script type="text/javascript">
        $(function () {
            $('#main_timepicker').timepicker(
            {
                'disableTimeRanges': [
                    ['12am', '8am'],
                    ['5pm', '11:59pm']
                ],
                'forceRoundTime': true,
                'scrollDefault': '8am',
            });
        });
    </script>
    <br />
	<!-- Applicant Information -->
	<div>
		<asp:Button ID="cmdContact" runat="server" Text="Contact Me" CssClass="button-primary" OnClientClick="javascript:alertStatus=false" OnClick="cmdContact_Click" />
    </div>
</asp:Content>
