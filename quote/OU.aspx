﻿<%@ Page Language="C#" MasterPageFile="~/App.master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance | Motorhome Insurance | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance, Motorhome Insurance, Blue Sky RV Insurance" />
	<meta http-equiv="Description" name="Description" content="Blue Sky RV has put years of RV insurance experience to work to offer specialty RV and motorhome insurance coverage policies for all types of RVs and motorhomes." />
	<style type="text/css">
		.text { 
			position: absolute;
			top: 130px;
			left: 560px;
			width: 395px;
			font-size: 14px;
		}
		.text h1 { font-size: 23px; margin-top: 10px; margin-bottom: 14px; }
		.text h2 { font-size: 18px; margin-top: -4px; margin-bottom: 1px; padding:0px; }
	</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="main" runat="server">
	<div class="content">
		<div class="company_info">
			<img src="images/default_bg2.jpg" alt="RV Insurance, Blue Sky RV Insurance" />
		</div>
		<div class="text">
			<h1>Welcome Outdoor Underwriters Policyholder</h1>
			<h2>Affordable Coverage for your Motorhome</h2>
			<p >Blue Sky has saved money for thousands of motorhome owners just like you.  Request your free quote today to see how much you could save on the insurance for your motorhome.</p>
			<h2>Innovative RV Insurance Products</h2>
			<p>The Blue Sky RV Insurance Program offers new and exciting specialty RV insurance coverages unavailable elsewhere.  Blue Sky is the first to offer a true depreciation free policy and the innovator of consignment coverage for your RV.</p>
			<img style="float: right; margin: -10px -5px 0 5px" src="images/ou_logo.png" alt="Outdoor Underwriters" />
			<h2>Financial Strength</h2>
			<p>Underwritten by Companion Property and Casualty Insurance Company, Rated “A” Excellent by A.M. Best, the Blue Sky RV Insurance Program provides products of the highest financial standards.</p>
		</div>
		<div class="quoteBanner"><a href="RV-Insurance-Quote-intro.aspx"></a></div>

	</div>
</asp:Content>
