<%@ Page Language="C#" MasterPageFile="~/App.master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>Blue Sky - The RV Insurance People</title>
	<style type="text/css">
		.text {
			position: absolute;
			top: 210px;
			left: 70px;
			width: 390px;
			font-size: 14px;
		}
		.text h1 {
			font-size: 30px;
			margin-top: 10px;
			margin-bottom: 14px;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">
	<div class="content">
		<div class="company_info">
			<img src="images/error_bg.jpg" alt="" />
		</div>
		<div class="text">
			<h1>AN ERROR HAS OCCURED</h1>
			I'm sorry but it appears an error has occured in our website. Please try your request again.		</div>
	</div>
</asp:Content>
