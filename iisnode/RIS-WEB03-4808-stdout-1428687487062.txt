GOGO
add route { path: 'venture/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Index Page HTML' }
add route { path: 'venture/products.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Products Page HTML' }
add route { path: 'venture/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters About Page HTML' }
add route { path: 'venture/atlanta.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Atlanta Page HTML' }
add route { path: 'venture/sanfrancisco.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters San Francisco Page HTML' }
add route { path: 'sterling/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Sterling Premium Finance Page HTML' }
add route { path: 'aui/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters Index Page HTML' }
add route { path: 'aui/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters About Page HTML' }
add route { path: 'aui/privacy.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters Privacy Page HTML' }
add route { path: 'auisb/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Specialty Brokerage Index Page HTML' }
add route { path: 'auisb/property.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Property B Index Page HTML' }
add route { path: 'auisb/casualty.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Casualty B Index Page HTML' }
add route { path: 'auisb/executiverisk.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Executive Risk B Index Page HTML' }
add route { path: 'auipl/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Personal Lines Index Page HTML' }
add route { path: 'auipl/homeowners.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Homeowners Index Page HTML' }
add route { path: 'auipl/autoinsurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Auto Index Page HTML' }
add route { path: 'auicondo/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Index Page HTML' }
add route { path: 'auisbs/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Small Business Solutions Index Page HTML' }
add route { path: 'auisbs/property.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Property Index Page HTML' }
add route { path: 'auisbs/casualty.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Casualty Index Page HTML' }
add route { path: 'auisbs/excessumbrella',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Excess Index Page HTML' }
add route { path: 'auisbs/inlandmarine.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Inland Marline Index Page HTML' }
add route { path: 'auisbs/liquorliability.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Liquor Liability Index Page HTML' }
add route { path: 'auisb/comingsoon.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Specialty Brokerage Coming Soon Page HTML' }
add route { path: 'auipl/comingsoon.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Personal Lines Coming Soon Page HTML' }
add route { path: 'auicondo/comingsoon.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Coming Soon Page HTML' }
add route { path: 'auisbs/comingsoon.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Small Business Solutions Coming Soon Page HTML' }
Serving on 127.0.0.1:\\.\pipe\c0721a53-ce55-4647-89a7-1f37e8742c84
(press ctrl-C to exit)
HIT /auisbs/liquorliability.html
pathprefix 
hit route { path: 'auisbs/liquorliability.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Liquor Liability Index Page HTML' }
HIT /auisbs/inlandmarine.html
pathprefix 
hit route { path: 'auisbs/inlandmarine.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Inland Marline Index Page HTML' }
HIT /auicondo/index.html
pathprefix 
hit route { path: 'auicondo/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Index Page HTML' }
HIT /auipl/index.html
pathprefix 
hit route { path: 'auipl/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Personal Lines Index Page HTML' }
HIT /auicondo/index.html
pathprefix 
hit route { path: 'auicondo/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Index Page HTML' }
HIT /auisbs/index.html
pathprefix 
hit route { path: 'auisbs/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Small Business Solutions Index Page HTML' }
HIT /auipl/index.html
pathprefix 
hit route { path: 'auipl/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Personal Lines Index Page HTML' }
HIT /auicondo/index.html
pathprefix 
hit route { path: 'auicondo/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Index Page HTML' }
