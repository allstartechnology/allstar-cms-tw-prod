GOGO
add route { path: 'venture/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Index Page HTML' }
add route { path: 'venture/products.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Products Page HTML' }
add route { path: 'venture/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters About Page HTML' }
add route { path: 'venture/atlanta.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Atlanta Page HTML' }
add route { path: 'venture/sanfrancisco.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters San Francisco Page HTML' }
add route { path: 'sterling/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Sterling Premium Finance Page HTML' }
add route { path: 'aui/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters Index Page HTML' }
add route { path: 'aui/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters About Page HTML' }
add route { path: 'auipl/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Personal Lines Index Page HTML' }
add route { path: 'auicondo/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Index Page HTML' }
add route { path: 'auiauto/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Auto Index Page HTML' }
add route { path: 'auihome/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Homeowners Index Page HTML' }
add route { path: 'auisb/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Specialty Brokerage Index Page HTML' }
add route { path: 'auipropb/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Property B Index Page HTML' }
add route { path: 'auicasualtyb/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Casualty B Index Page HTML' }
add route { path: 'auiexecb/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Executive Risk B Index Page HTML' }
add route { path: 'auisbs/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Small Business Solutions Index Page HTML' }
add route { path: 'auiprop/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Property Index Page HTML' }
add route { path: 'auicas/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Casualty Index Page HTML' }
add route { path: 'auieu/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Excess Index Page HTML' }
add route { path: 'auiim/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Inland Marline Index Page HTML' }
add route { path: 'auill/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Liquor Liability Index Page HTML' }
add route { path: 'auitrans/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Transportation Index Page HTML' }
Serving on 127.0.0.1:\\.\pipe\3109be6b-7e36-40e4-ad7b-d4c3dc7f15ee
(press ctrl-C to exit)
HIT /TestDir/AllstarCMS/auihome/index.html
pathprefix /TestDir/AllstarCMS
hit route { path: 'auihome/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Homeowners Index Page HTML' }
