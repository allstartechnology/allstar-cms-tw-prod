GOGO
add route { path: 'robots.txt',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Robots - Root' }
add route { path: 'venture/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Index Page HTML' }
add route { path: 'venture/products.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Products Page HTML' }
add route { path: 'venture/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters About Page HTML' }
add route { path: 'venture/atlanta.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters Atlanta Page HTML' }
add route { path: 'venture/sanfrancisco.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Venture Underwriters San Francisco Page HTML' }
add route { path: 'sterling/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Sterling Premium Finance Page HTML' }
add route { path: 'aui/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters Index Page HTML' }
add route { path: 'aui/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters About Page HTML' }
add route { path: 'aui/privacy.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters Privacy Page HTML' }
add route { path: 'aui/kj_contest.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Allstar Underwriters KJ Contest Page HTML' }
add route { path: 'aui/thanks.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Thanks Page HTML' }
add route { path: 'aui/transportation.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Transportation Index Page HTML' }
add route { path: 'aui/robots.txt',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Robots' }
add route { path: 'aui/sitemap.xml',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Sitemap' }
add route { path: 'auisb/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Specialty Brokerage Index Page HTML' }
add route { path: 'auisb/property.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Property B Index Page HTML' }
add route { path: 'auisb/casualty.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Casualty B Index Page HTML' }
add route { path: 'auisb/executiverisk.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Executive Risk B Index Page HTML' }
add route { path: 'auipl/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Personal Lines Index Page HTML' }
add route { path: 'auipl/homeowners.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Homeowners Index Page HTML' }
add route { path: 'auipl/home_thanks.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Homeowners Thanks Page HTML' }
add route { path: 'auipl/autoinsurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Auto Index Page HTML' }
add route { path: 'auipl/agent_form.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Agent Form Page HTML' }
add route { path: 'auipl/auto_thanks.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Auto Thanks Page HTML' }
add route { path: 'auipl/umbrellainsurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Umbrella Index Page HTML' }
add route { path: 'auitransportation/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Transportation Index Page HTML' }
add route { path: 'auicondo/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Index Page HTML' }
add route { path: 'auicondo/robots.txt',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Robots' }
add route { path: 'auicondo/sitemap.xml',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Sitemap' }
add route { path: 'auicondo/agent_form.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Agent Form Page HTML' }
add route { path: 'auicondo/contact_form.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Contact Form Page HTML' }
add route { path: 'auicondo/sbs.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo SBS Page HTML' }
add route { path: 'auicondo/excess.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Excess Page HTML' }
add route { path: 'auicondo/umbrella.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Umbrella Page HTML' }
add route { path: 'auicondo/contact_thanks.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Thanks Page HTML' }
add route { path: 'auicondo/agent_thanks.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Condo Thanks Page HTML' }
add route { path: 'auisbs/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Small Business Solutions Index Page HTML' }
add route { path: 'auisbs/property.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Property Index Page HTML' }
add route { path: 'auisbs/casualty.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Casualty Index Page HTML' }
add route { path: 'auisbs/excessumbrella',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Excess Index Page HTML' }
add route { path: 'auisbs/inlandmarine.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Inland Marline Index Page HTML' }
add route { path: 'auisbs/liquorliability.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI Liquor Liability Index Page HTML' }
add route { path: 'auisbs/inhomebusiness.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'AUI In-Home Index Page HTML' }
add route { path: 'arkon/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon Index Page HTML' }
add route { path: 'arkon/contact.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon Contact Page HTML' }
add route { path: 'arkon/products.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon Products Page HTML' }
add route { path: 'arkon/general_liability.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon GenLiab Page HTML' }
add route { path: 'arkon/excess_liability.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon ExcessLiab Page HTML' }
add route { path: 'arkon/prop_inland.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon PropInland Page HTML' }
add route { path: 'arkon/environmental.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon Environmental Page HTML' }
add route { path: 'arkon/resources.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon Resources Page HTML' }
add route { path: 'arkon/robots.txt',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Arkon Robots' }
add route { path: 'aspen/index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Index Page HTML' }
add route { path: 'aspen/agents.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Agents Page HTML' }
add route { path: 'aspen/auto_insurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Auto Insurance Page HTML' }
add route { path: 'aspen/claims.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Claims Page HTML' }
add route { path: 'aspen/claims_information.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Claims Information Page HTML' }
add route { path: 'aspen/contact.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Contact Page HTML' }
add route { path: 'aspen/about.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen About Page HTML' }
add route { path: 'aspen/behind_the_name.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Behind the Name Page HTML' }
add route { path: 'aspen/giving_back.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Giving Back Page HTML' }
add route { path: 'aspen/allstar_fg.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Allstar FG Page HTML' }
add route { path: 'aspen/careers.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Careers Page HTML' }
add route { path: 'aspen/news_room.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen News Room Page HTML' }
add route { path: 'aspen/robots.txt',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Robots' }
add route { path: 'aspen/sitemap.xml',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Aspen Sitemap' }
add route { path: 'index.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Index Page HTML' }
add route { path: 'rv_insurance_coverages.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Coverages Page HTML' }
add route { path: 'innovative_rv_insurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Innovative Page HTML' }
add route { path: 'specialty_rv_insurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Specialty Page HTML' }
add route { path: 'specialty_total_loss_replacement.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Total Loss Sp Page HTML' }
add route { path: 'specialty_diminishing_deductible.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Diminishing Deductible Sp Page HTML' }
add route { path: 'specialty_personal_effects.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Personal Effects Sp Page HTML' }
add route { path: 'specialty_full_timer.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Full Timer Sp Page HTML' }
add route { path: 'specialty_vacation_liability.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Vacation Liability Sp Page HTML' }
add route { path: 'specialty_emergency_expense.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Emergency Expense Sp Page HTML' }
add route { path: 'specialty_awning_replacement.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Awning Replacement Sp Page HTML' }
add route { path: 'specialty_purchase_price_guarantee.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Purchase Price Sp Page HTML' }
add route { path: 'specialty_scheduled_medical_payments.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Medical Payments Sp Page HTML' }
add route { path: 'specialty_adjacent_structures.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Adjacent Structures Sp Page HTML' }
add route { path: 'specialty_agreed_value.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Agreed Value Sp Page HTML' }
add route { path: 'standard_rv_insurance.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Standard Page HTML' }
add route { path: 'vehicles',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Vehicles Page HTML' }
add route { path: 'class_a_motorhome.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Class A Page HTML' }
add route { path: 'class_c_motorhome.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Class C Page HTML' }
add route { path: 'camper_van.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Camper Van Page HTML' }
add route { path: 'bus_conversion.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Bus Conversion Page HTML' }
add route { path: 'medium_duty_tow_vehicle.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Medium Tow Page HTML' }
add route { path: 'toter_home.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Toterhome Page HTML' }
add route { path: 'conventional_travel_trailer.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Conventional Page HTML' }
add route { path: 'fifth_wheel_camper.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Fifth Wheel Page HTML' }
add route { path: 'pop_up_camper.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Pop Up Page HTML' }
add route { path: 'truck_camper.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Truck Camper Page HTML' }
add route { path: 'park_model.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Park Model Page HTML' }
add route { path: 'utility_trailer.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Utility Page HTML' }
add route { path: 'claims.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Claims Page HTML' }
add route { path: 'about_bluesky.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky About Page HTML' }
add route { path: 'represent.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Represent Page HTML' }
add route { path: 'license.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky License Page HTML' }
add route { path: 'contact_bluesky.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Contact Page HTML' }
add route { path: 'personal_auto.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Personal Auto Page HTML' }
add route { path: 'bluesky_privacy_policy.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky Privacy Policy Page HTML' }
add route { path: 'rv_trader.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky RV Trader Page HTML' }
add route { path: 'bluesky_oui.html',
  serveType: 'text/html',
  renderType: 'text/plain',
  rootTiddler: 'Blue Sky OUI Referral Page HTML' }
Serving on 127.0.0.1:\\.\pipe\47f49d38-42c9-4efe-8d69-b48670db03ca
(press ctrl-C to exit)
HIT /
pathprefix 
GET ROOT
HIT /status
pathprefix 
HIT /recipes/default/tiddlers.json
pathprefix 
HIT /favicon.ico
pathprefix 
